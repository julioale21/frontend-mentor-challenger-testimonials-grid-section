# Frontend Mentor Challenger - Testimonials grid section

Frontend mentor challenger![Testimonials_grid_section_coding_challenge](/uploads/2a917837b7d96618014da18593ba43fb/Testimonials_grid_section_coding_challenge.png)
https://www.frontendmentor.io/challenges/testimonials-grid-section-Nnw6J7Un7

##### Description:
Grid section made with html and Bootstrap 5. 

Sass was used to customize Bootstrap.

Deployed on netlify.

##### Deploy:
Deployed challenger: https://frontend-mentor-testimonials.netlify.app/
